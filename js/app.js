document.querySelector('#generate-name').addEventListener('submit', loadNames);


function loadNames(e){
    e.preventDefault();
    const country = document.getElementById('country');
    const selectedCountry = country.options[country.selectedIndex].value;

    const gender = document.getElementById('gender');
    const selectedGender = gender.options[gender.selectedIndex].value;

    const amount = document.getElementById('amount').value;
    let url = '';
    url += 'http://uinames.com/api/?';

    if(selectedCountry !== ''){
        url += `region=${selectedCountry}&`
    }
    if(selectedGender !== ''){
        url += `gender=${selectedGender}&`
    }
    if(amount !== ''){
        url += `amount=${amount}&`
    }

    fetch(url)
        .then(res => res.json())
        .then(names => {
            let html = '<h5>Random names</h5>';
            html += '<ul class="list">'
            names.forEach(function(name) {
                html += `
                <p>${name.name} ${name.surname}</p>`
            });
            document.getElementById('response').innerHTML = html;
        })
        .catch(error => console.log(error))
}
